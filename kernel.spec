Name: kernel
Summary: The Linux Kernel
Version: 5.11.4_tkg_bmq+
Release: 1
License: GPL
Group: System Environment/Kernel
Vendor: The Linux Community
URL: https://www.kernel.org
Source: kernel-5.11.4_tkg_bmq+.tar.gz
Provides:  kernel-5.11.4_tkg_bmq+
BuildRequires: python3-devel gcc make perl-generators perl-interpreter openssl-devel bison flex findutils git-core perl-devel openssl elfutils-devel gawk binutils m4 tar hostname bzip2 bash gzip xz bc diffutils redhat-rpm-config net-tools elfutils patch rpm-build dwarves kmod libkcapi-hmaccalc perl-Carp rsync grubby pesign
%define __spec_install_post /usr/lib/rpm/brp-compress || :
%define debug_package %{nil}

%description
The Linux Kernel, the operating system core itself

%package headers
Summary: Header files for the Linux kernel for use by glibc
Group: Development/System
Obsoletes: kernel-headers < %{version}
Provides: kernel-headers = %{version}
%description headers
Kernel-headers includes the C header files that specify the interface
between the Linux kernel and userspace libraries and programs.  The
header files define structures and constants that are needed for
building most standard programs and are also needed for rebuilding the
glibc package.

%package devel
Summary: Development package for building kernel modules to match the 5.11.4_tkg_bmq+ kernel
Group: System Environment/Kernel
AutoReqProv: no
%description devel
This package provides kernel headers and makefiles sufficient to build modules
against the 5.11.4_tkg_bmq+ kernel package.

%prep
%setup -q

%build
make %{?_smp_mflags} KBUILD_BUILD_VERSION=%{release}

%install
mkdir -p %{buildroot}/boot
%ifarch ia64
mkdir -p %{buildroot}/boot/efi
cp $(make image_name) %{buildroot}/boot/efi/vmlinuz-5.11.4_tkg_bmq+
ln -s efi/vmlinuz-5.11.4_tkg_bmq+ %{buildroot}/boot/
%else
cp $(make image_name) %{buildroot}/boot/vmlinuz-5.11.4_tkg_bmq+
%endif
make %{?_smp_mflags} INSTALL_MOD_PATH=%{buildroot} modules_install
make %{?_smp_mflags} INSTALL_HDR_PATH=%{buildroot}/usr headers_install
cp System.map %{buildroot}/boot/System.map-5.11.4_tkg_bmq+
cp .config %{buildroot}/boot/config-5.11.4_tkg_bmq+
bzip2 -9 --keep vmlinux
mv vmlinux.bz2 %{buildroot}/boot/vmlinux-5.11.4_tkg_bmq+.bz2
rm -f %{buildroot}/lib/modules/5.11.4_tkg_bmq+/build
rm -f %{buildroot}/lib/modules/5.11.4_tkg_bmq+/source
mkdir -p %{buildroot}/usr/src/kernels/5.11.4_tkg_bmq+
tar cf - --exclude SCCS --exclude BitKeeper --exclude .svn --exclude CVS --exclude .pc --exclude .hg --exclude .git --exclude=*vmlinux* --exclude=*.mod --exclude=*.o --exclude=*.ko --exclude=*.cmd --exclude=Documentation --exclude=.config.old --exclude=.missing-syscalls.d --exclude=*.s . | tar xf - -C %{buildroot}/usr/src/kernels/5.11.4_tkg_bmq+
cd %{buildroot}/lib/modules/5.11.4_tkg_bmq+
ln -sf /usr/src/kernels/5.11.4_tkg_bmq+ build
ln -sf /usr/src/kernels/5.11.4_tkg_bmq+ source

%clean
rm -rf %{buildroot}

%post
if [ -x /sbin/installkernel -a -r /boot/vmlinuz-5.11.4_tkg_bmq+ -a -r /boot/System.map-5.11.4_tkg_bmq+ ]; then
cp /boot/vmlinuz-5.11.4_tkg_bmq+ /boot/.vmlinuz-5.11.4_tkg_bmq+-rpm
cp /boot/System.map-5.11.4_tkg_bmq+ /boot/.System.map-5.11.4_tkg_bmq+-rpm
rm -f /boot/vmlinuz-5.11.4_tkg_bmq+ /boot/System.map-5.11.4_tkg_bmq+
/sbin/installkernel 5.11.4_tkg_bmq+ /boot/.vmlinuz-5.11.4_tkg_bmq+-rpm /boot/.System.map-5.11.4_tkg_bmq+-rpm
rm -f /boot/.vmlinuz-5.11.4_tkg_bmq+-rpm /boot/.System.map-5.11.4_tkg_bmq+-rpm
fi

%preun
if [ -x /sbin/new-kernel-pkg ]; then
new-kernel-pkg --remove 5.11.4_tkg_bmq+ --rminitrd --initrdfile=/boot/initramfs-5.11.4_tkg_bmq+.img
elif [ -x /usr/bin/kernel-install ]; then
kernel-install remove 5.11.4_tkg_bmq+
fi

%postun
if [ -x /sbin/update-bootloader ]; then
/sbin/update-bootloader --remove 5.11.4_tkg_bmq+
fi

%files
%defattr (-, root, root)
/lib/modules/5.11.4_tkg_bmq+
%exclude /lib/modules/5.11.4_tkg_bmq+/build
%exclude /lib/modules/5.11.4_tkg_bmq+/source
/boot/*

%files headers
%defattr (-, root, root)
/usr/include

%files devel
%defattr (-, root, root)
/usr/src/kernels/5.11.4_tkg_bmq+
/lib/modules/5.11.4_tkg_bmq+/build
/lib/modules/5.11.4_tkg_bmq+/source
